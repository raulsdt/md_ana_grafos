import spotipy
import json
from collections import deque
import spotipy.util as util

scope = 'user-library-read'

username = 'username'
token = util.prompt_for_user_token(username, scope)

def track_transform(track,input):
    track_transform = {}
    track_transform['type'] = track['type']
    track_transform['name'] = track['name']
    track_transform['id'] = input
    #track_transform['id'] = track['id']
    track_transform['popularity'] = track['popularity']
    track_transform['duration_ms'] = track['duration_ms']
    track_transform['uri'] = track['uri']
    track_transform['url'] = track['external_urls']['spotify']

    return track_transform

def artist_iterative(sp, artist_id, max_level):

    with open('out_def.json', 'w', newline='') as f:
        print('{"directed": false, "graph": {}, "nodes": [',file=f)

        node_num = 0

        artists_dict = dict()
        songs_dict = dict()
        link_list = list()
        artists = set()
        q = deque()

        q.append(artist_id)
        q.append("VACIO")
        level = 0
        first = True
        while len(q) > 0:
            try:
                print('----- LEVEL -----', level, '----- q -----', len(q), '---- node_num ----', node_num)
                id = q.popleft()
                if id == "VACIO":
                    if(level <= max_level):
                        level += 1
                    continue



                artist_ind = sp.artist(id)

                if not artist_ind['name'] in artists_dict :
                    if not first:
                        print(',', file=f)
                    first = False
                    artist_ind['id']=node_num
                    print(json.dumps(artist_ind), file=f) # Impresión de nodos artista
                    artists_dict[artist_ind['name']] = node_num
                    node_num = node_num + 1


                toptracks_artist = sp.artist_top_tracks(id, country='ES')

                for track in toptracks_artist['tracks']:

                    if len(track['artists']) > 1:
                        if not track['name'] in songs_dict:
                            songs_dict[track['name']] = node_num


                            print(',', file=f)
                            print(json.dumps(track_transform(track,node_num)), file=f)
                            node_num = node_num + 1
                        for artist_el in track['artists']:

                            if artist_el['name'] in artists_dict: # Existe el artista en nuestro diccionario
                                link = {} # Se establece la relación
                                link['source'] = artists_dict[artist_el['name']]
                                link['target'] = songs_dict[track['name']]
                                link_list.append(link)
                            else: # No existe el artista en nuestro diccionario
                                if level <= max_level:
                                    q.append(artist_el['id'])
                                artists.add(artist_el['name'])

                                print(',',file=f)

                                # Llamando a artist.
                                artist_to_save = sp.artist(artist_el['id'])
                                artist_to_save['id'] = node_num
                                print(json.dumps(artist_to_save), file=f) # Impresión de nodos artista
                                artists_dict[artist_el['name']] = node_num # Introducimos artista en dicctionario
                                node_num = node_num + 1
                                link = {}
                                link['source'] = artists_dict[artist_el['name']]
                                link['target'] = songs_dict[track['name']]
                                link_list.append(link)

                if level <= max_level:
                    # related = sp.artist_related_artists(id)
                    # for a in related['artists']:
                    #     if a['name'] not in artists:
                    #         q.append(a['id'])
                    #         artists.add(a['name'])
                    q.append("VACIO")
            except:
                sp = spotipy.Spotify(auth=token)

        # Imprimimos todas las relaciones
        print('],"links":[', file=f)
        first2 = True
        for song in link_list:
            if not first2:
                print(',',file=f)
            first2=False
            print(json.dumps(song),file=f)
        print('], "multigraph":false}',file=f)




if __name__ == '__main__':
    artist_id = '6eUKZXaKkcviH0Ku9w2n3V'
    sp = spotipy.Spotify(auth=token)
    artists = set()

    artist_iterative(sp, artist_id, 3)
