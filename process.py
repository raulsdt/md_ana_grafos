import networkx as nx
from networkx.readwrite import json_graph
import json
import community
import operator



with open('out_def.json', 'r') as f:
    # 1 Carga el grafo y visualizacion
    json_data = json.load(f)
    g = json_graph.node_link_graph(json_data, directed=False)


    #2 Muestra todos los artistas
    print("ARTISTAS")
    for node, atts in g.nodes(data=True):
        if 'type' in atts and atts['type'] == 'artist':
            print(node)


    total = {}
    # #ok Coeficiente de clustering
    print("\nCOEFICIENTE DE CLUSTERING")
    print('Clustering:', nx.average_clustering(g))

    print("\nCOMPONENTES CONEXAS")
    i = 1
    for comp in nx.connected_component_subgraphs(g):
        print('Comp ', i, ': ')
        i += 1
        for node in comp.nodes():
            print(node, '\t')
        print('\tSize:', len(comp.nodes()))
        if len(comp.nodes()) > 1:

            degreeCent = nx.degree_centrality(comp)
            closeness = nx.closeness_centrality(comp)
            betweenness = nx.betweenness_centrality(comp)
            pagerank = nx.pagerank(comp)
            print('\tDegree:', degreeCent)
            print('\tMAX DEG: ', max(degreeCent.items(), key=operator.itemgetter(1)))
            print('\tCloseness:', closeness)
            print('\tMAX CLOSENESS: ', max(closeness.items(), key=operator.itemgetter(1)))
            print('\tBetweenness:', betweenness)
            print('\tMAX BETWEENNESS: ', max(betweenness.items(), key=operator.itemgetter(1)))
            print('\tPageRank:', pagerank)
            print('\tMAX PAGERANK: ', max(pagerank.items(), key=operator.itemgetter(1)))
            total.update(pagerank)
    print('ALL PAGERANK:  ', total)

    for element in total:
        g.node[int(element)]['pagerank'] = total[element]
    #nx.set_node_attributes(g, 'pagerank', total)



    print("\nDETECCIÓN DE COMUNIDADES")
    communities = community.best_partition(g)
    print(communities)
    for element in communities:
        g.node[int(element)]['community'] = communities[element]

    with open('out_process.json', 'w') as f:
        g_json = json_graph.node_link_data(g)
        json.dump(g_json, f, indent=1)


    #print(sorted(map(sorted,top_level_communities))) # Problema con sorted por el tipo de dato devuelto en top_level_comunities (str and int)